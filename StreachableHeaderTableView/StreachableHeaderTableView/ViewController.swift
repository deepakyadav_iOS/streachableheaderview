//
//  ViewController.swift
//  StreachableTableView
//
//  Created by Apple on 01/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate {
   
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var saveTranheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var savetransacLabel: UILabel!
    @IBOutlet weak var monthHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var transaction_TableView: UITableView!
    @IBOutlet weak var monthLable: UILabel!
    @IBOutlet weak var animatedView : UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        transaction_TableView.dataSource = self
        transaction_TableView.delegate = self
        transaction_TableView.contentInset = UIEdgeInsets(top: 350, left: 0, bottom: 0, right: 0)
        
        let gradientView = GradientView(frame: self.animatedView.bounds)
        self.animatedView.insertSubview(gradientView, at: 0)
       
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))

        let headerlabel = UILabel()
        headerlabel.frame = CGRect.init(x: 10, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        headerlabel.text = "Thurasday 22 may"
        headerlabel.font = UIFont(name: "Helvetica-bold", size: 22)
        //        label.textColor = UIColor.charcolBlackColour() // my custom colour

        headerView.addSubview(headerlabel)
        //        self.tableView.tableHeaderView = headerView


        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 20
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionTableViewCell
        cell.tittle_label.text = "Marina Bay Stands"
        cell.description_Label.text = "Eating out"
        cell.price_Label.text = " $28.00"
     
        return cell
       }
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let y = -scrollView.contentOffset.y
       let height = min(max(y, 100), 350)
        heightConstraint.constant = height
        print("height---",height)
        if height == 100 {
            saveTranheightConstraint.constant = 0
            monthHeightConstraint.constant = 0

        } else {
            saveTranheightConstraint.constant = 21
            monthHeightConstraint.constant = 21
        }
        self.view.layoutIfNeeded()
        
    }
       
}


