//
//  TransactionTableViewCell.swift
//  StreachableHeaderTableView
//
//  Created by Apple on 03/04/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    @IBOutlet weak var purchase_imageView: UIImageView!
      
      @IBOutlet weak var tittle_label: UILabel!
      @IBOutlet weak var description_Label: UILabel!
      @IBOutlet weak var price_Label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
